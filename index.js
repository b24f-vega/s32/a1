const http = require('http');

const port = 3000;

const server = http.createServer( (request, response) => {
    if(request.url == '/' && request.method == 'GET'){
        response.writeHead(200, {'Content-type': 'text/plain'});
        response.end('Welcome to booking system');
    }
    if(request.url == '/profile' && request.method == 'GET'){
        response.writeHead(200, {'Content-type': 'text/plain'});
        response.end('Welcome to your prfoile');
    }
    if(request.url == '/courses' && request.method == 'GET'){
        response.writeHead(200, {'Content-type': 'text/plain'});
        response.end('Here\'s our courses available');
    }
    if(request.url == '/addcourse' && request.method == 'POST'){
        response.writeHead(200, {'Content-type': 'text/plain'});
        response.end('Add a course to our resources');
    }
    if(request.url == '/updatecourse' && request.method === 'PUT'){
        response.writeHead(200, {'Content-type': 'text/plain'});
        response.end('Update a course to our resources');
    }
    if(request.url == '/archivecourse' && request.method === 'DELETE'){
        response.writeHead(200, {'Content-type': 'text/plain'});
        response.end('Archive courses to our resources');
    }
}).listen(port);

console.log(`Server now accessible at localhost: ${port}`);